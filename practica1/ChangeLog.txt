Practia 1
-- ChangeLog.txt creado.
-- Cambiado el nombre de practica4 a practica5.
-- Esfera.cpp modificado.

Practica 2
-- Creado el branch sobre el que haré la práctica.
-- Creado disminución de pelota.
-- Creado movimiento raqueta modificando la función Mueve.
-- Creado movimiento pelota.

Practica 3
-- Creado el branch de la páctica 3.
-- Modificado el archivo Mundo.h para añadir la tubería y archivos de librería necesarios para trabajar con tuberías.
-- Añadida la tubería en Mundo.cpp.
-- Añadido el mensaje de la tubería en Mundo.cpp.
-- Añadido el cierre de la tubería en el destructor del tenis.
-- En la creación del bot, se crea el archivo DatosMemCompartida.h dentro del directorio include.
-- Modificado el archivo CMakeList para incluir el bot.
-- Creación del atributo de memoria compartida en Mundo.h.
-- Creación del bot.
-- Añadida la finalización del juego por límite de puntos para el ganador.

Practica 4
-- Añadido cliente y servidor.
-- Corregidos algunos errores y eliminado elementos no necesarios.
-- Practica 4 terminada.

Practica 5
-- Creado el branch de la práctica 5.
-- Incluidos Socket.h y Socket.cpp.
-- Modificado el archivo CMakeList incorporando el socket.
-- Retocados los archivos MundoCliente.cpp, MundoCliente.h, MundoServidor.cpp y MundoServidor.h.
-- La comunicacion por sockets funciona.
